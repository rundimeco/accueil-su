# Les sites physiques de Sorbonne

Descriotion des salles informatiques détaillées (avec notamment le cahier technique recensant les outils installés) ici : https://ent.sorbonne-universite.fr/lettres-personnels/fr/din/salles-informatiques.html
## Serpente

Emplacement : [28 rue Serpente, 75006 Paris](https://fr.mappy.com/#/1/M2/TSearch/S28+Rue+Serpente%2C+75006+Paris/N151.12061,6.11309,2.34187,48.85246/Z16/) (Métro St Michel [ligne 4 ou RER B/C] ou Odéon [ligne 10])

Pour les salles, demander la clé à l'accueil

Les principales salles à connaître :
- Salle 219/220: salle informatique (Windows + VM Linux)
- Salle 318: salle informatique (Windows + VM Linux ?)
- Bureau 205: Victoria
- Bureau 206: doctorants et ATER
- Bureau 207: Anastazija et Shehrazade (aka bureau des princesses)
- Bureau 210: Claude et Laurence
- Bureau 211: Gaël
- Bureau 212: "Open Space" (Jean-Claude, Françoise + collègues de socio)

## Malesherbes

Emplacement : [108 boulevard Malesherbes, 75017 Paris](https://fr.mappy.com/#/2/M2/TGeoentity/F532105b2e4b07c1cacaf81a0/N151.12061,6.11309,2.30953,48.88343/Z15/)

(Métro Malesherbes [ligne 3])
Activité principale : enseignements.

Demander la clé de la salle à l'accueil contre son badge.

- Salle 220 [Pix uniquement] (2ème étage, escalier à droite quand on est face à l'accueil). les codes des machines sont affichés au fond de la salle (différents pour les sessions Windows et Linux)
- Salles BS17/BS18 aka Esperanto (sous-sol, escalier à gauche quand on est face à l'accueil): Salles infos

Pour les TD de Pix, une pochette à votre nom, comprenant TD et liste des étudiants, sera à récupérer dans le casier du Pix à l’accueil du centre.

Salle des professeurs (sert aussi pour repas de midi) :
- salle 335
- demander le code aux sages ;)

## porte de Clignancourt

Emplacement : [2 Rue Francis de Croisset, 75018 Paris](https://fr.mappy.com/#/5/M2/TSearch/S2+Rue+Francis+de+Croisset%2C+75018+Paris/N151.12061,6.11309,2.34735,48.89958/Z16/)

(métro Clignancourt [ligne 4])
Activité principale : enseignements.
- CM Pix
- TD Pix

Sophie laisse des pochettes à vos noms, comprenant, TD et CM, que vous trouverez dans la salle de reprographie des enseignants (étage :  -1 ;   à côté du service de la reprographie).

Demander la fabrication d'un badge pour accéder à l'ascenseur et à la salle de reprographie au sous-sol. UFR sociologie, accès Pix :
- demander la fabrication du badge à la cellule PIX (DominiqueChapelier ou via Christian Vincent).

Pour les salles :
- les amphis sont ouverts par le personnel aux horaires des cours, ils sont fermés sinon. Il n'y a rien à faire de notre côté.
- il faut demander les clés des salles de TD dans le bureau de Bouygues, au fond du couloir sur la droite juste après le PC sécurité.
- Code des ordinateurs affichés dans la salle (TODO:vérifier)

Salle de repos / repas le midi au x-ième étage.

# Organisation générale 

## Contacts administratifs

### Pix
- SARNOUK Jennifer (jennifer.sarnouk@sorbonne-universite.fr)
 - Malesherbes, Bureau A316 (3ème étage ), 01.43.18.40.62
 - Clignancourt, Bureau 508 (5ème étage), 01.53.09.56.06

### Serpente (Licence et Master, secrétariat de l'UFR)

- Anastazija Gavrilov, secrétariat Licence-Master (Anastazija.Dzerdz-Gavrilov@sorbonne-universite.fr), Serpente 206, Lun-Ven 9h-17h  (01.53.10.58.30)
- Shehrazade Lakaf, responsable administrative de l'UFR (Shahrazed.Lakaf@sorbonne-universite.fr), Serpente 206, Lun-Ven 8h-16h [à vérifier]  (01.53.10.58.26)

## Enseignement

- Master: responsables Laurence Devillers et Claude Montacié
- Licence: responsable Victoria Eyharabide
- Pix (ex C2I): Jean Claude Pena

- Direction de l'UFR (renouvellement en février 2025): Renaud Debailly (directeur), Gaël Lejeune et Claude Montacié (directeurs adjoints)


## Recherche

- Séminaire mensuel de l'équipe Informatique : http://stih-sorbonne-universite.fr/dokuwiki/doku.php?id=seminaire_lc)
- Groupe de Lecture ML4DH :https://obtic.sorbonne-universite.fr/actualite/club-de-lecture-ml4dh-appel-a-participation/

Pour le financements de missions, les ATER peuvent demander à Claude Montacié et/ou aux collègues qui ont des contrats

# Compte Sorbonne

## Activer son compte Sorbonne

- aller sur [cassal](https://cassal-2.paris-sorbonne.fr/cas/login?service=https%3A%2F%2Fmoodle.paris-sorbonne.fr%2Flogin%2Findex.php%3FauthCAS%3DCAS)
- Première connexion ou perte du mot de passe ? ==> Enseignant ou personnel
- identifiant ==> "v" + numéro à l'envers du badge
- l'intégration d'un mot de passe prend 2 à 7 jours (normalement 2), important de le faire rapidement.

## Les sites internet importants

- [Mail Sorbonne](https://zcs.sorbonne-universite.fr/mail)
- [Moodle Sorbonne](https://moodle.paris-sorbonne.fr/)
- [intranet Sorbonne](https://intranet.sorbonne-universite.fr/)
- [portail wifi Sorbonne](https://portail.wifi.univ-paris4.fr:8001/index.php?redirurl=http%3A%2F%2Fwww.sorbonne-universite.fr%2F)

# Imprimantes

## Serpente

Imprimante du couloir 2ème étage (obsolète depuis le 5 décembre 2022, à corriger):
- emplacement : 172.20.12.33
- URI : socket://172.20.12.33:9100
- modèle : Toshiba e-Studio 162

Nouvelle imprimante (à tester) : C-SE-2eme sur 172.30.0.61


# Cours
- Les codes des cours: 1 lettre pour licence/Master, 2 lettres pour le nom de l'UFR, 1 chiffre pour le numéro du semestre, 4 caractères pour le nom du cours (motivé pour la licence, nom motivé pour le master:
- L1SOPROG: L=Licence, 1 = Semestre 1, SO = Sociologie et Informatique, PROG = programmation
- L5SOPROG: L=Licence, 5 = Semestre 5 (=3ème année)", SO = Sociologie ....
- M2SOL033: M=Master, 2 = Semestre 2, So = Sociologie, L033 on sait pas pourquoi (Web Sémantique et Big data)

D'où S4 = Semestre 2 de la L2

## PIX

[Moodle du PIX](https://moodle.paris-sorbonne.fr/course/view.php?id=10979)


## Licence Sciences du Langage (SDL)

- UE3 (tronc commun)
- UE5 (option): programmation Python sauf S4: Gate
- [Moodle partie Info licence SDL](https://moodle.paris-sorbonne.fr/course/index.php?categoryid=85)

## Master Langue et informatique
(Tronc commun linguistique partagé avec les autres master SDL)
- Partie Parole (C.Montacié)
- Partie TAL (G.Lejeune)
- Partie Connaissances (V.Eyharabide)
- Autres domaines : L.Devillers (Informatique générale), F.Guerin (Linguistique), P.Boldini (Logique)
- [Moodle Master Info](https://moodle.paris-sorbonne.fr/course/index.php?categoryid=411)

# Administratif

## Contact RH
Alexandra Zenati du service RH (alexandra.zenati@sorbonne-universite.fr), si pas de réponse: en parler à Shehrazade à l'UFR

## Signature du Contrat
- Venir avec une photo d'identité et la carte professionnelle est faite en même temps.
- Pour rentrer dans les bâtiments sans la carte pro: demander un mail de la part d'un des collègues.
- Venir avec les originaux des fiche de renseignement et demande de prise en charge des frais de transport (même si envoyés par mail).
